import { Routes } from '@angular/router';
import { TODOLIST_ROUTES } from './todolist/todolist.routes';

export const APP_ROUTES: Routes = [
  { path: '', children: TODOLIST_ROUTES }
];