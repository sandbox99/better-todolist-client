import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TodolistPageComponent } from './pages/todolist-page/todolist-page.component';
import { TodoListComponent } from './components/todo-list.component';
import { TaskComponent } from './components/task.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [TodolistPageComponent, TodoListComponent, TaskComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class TodolistModule { }
