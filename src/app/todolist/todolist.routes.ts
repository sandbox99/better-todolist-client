import { TodoListResolver } from './shared/resolvers/todo-list.resolver';
import { TodolistPageComponent } from './pages/todolist-page/todolist-page.component';
import { Routes } from '@angular/router';

export const TODOLIST_ROUTES: Routes = [
  { 
    path: '', 
    component: TodolistPageComponent,
    resolve: {
      todolists: TodoListResolver
    }
  }
];