import { TaskApi } from '../api/task.api';
import { Task } from './../models/task';
import { TodoListApi } from '../api/todo-list.api';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { TodoList } from '../models/todo-list';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {

  constructor(
    private todolistApi: TodoListApi,
    private taskApi: TaskApi
  ) { }

  getAll(): Observable<TodoList[]> {
    return this.todolistApi.getAll();
  }

  addTask(task: Task, todolist: TodoList): Observable<Task> {
    task.todolist = todolist['@id'];
    return this.taskApi.create(task);
  }

  checkTask(task: Task, todolist: TodoList): Observable<Task> {
    task.todolist = todolist['@id'];
    let patchTask: Task = {
      id: task.id,
      done: task.done,
      todolist: todolist['@id']
    }
    return this.taskApi.patch(patchTask);
  }
}
