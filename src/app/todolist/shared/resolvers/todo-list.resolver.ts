import { TodoListService } from './../services/todo-list.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TodoList } from '../models/todo-list';

@Injectable({
  providedIn: 'root'
})
export class TodoListResolver implements Resolve<TodoList[]> {

  constructor(private todoListService: TodoListService) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TodoList[]> {
    return this.todoListService.getAll();
  }
}
