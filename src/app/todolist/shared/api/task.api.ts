import { Observable } from 'rxjs';
import { Task } from './../models/task';
import { Injectable } from '@angular/core';
import { environment } from './../../../../environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TaskApi {

  private ressourceUrl: string = `${environment.apiUrl}/tasks`;

  constructor(private http: HttpClient) { }

  create(task: Task): Observable<Task> {
    return this.http.post<Task>(this.ressourceUrl, task);
  }

  patch(task: Task): Observable<Task> {
    return this.http.patch<Task>(`${this.ressourceUrl}/${task.id}`, task);
  }
}
