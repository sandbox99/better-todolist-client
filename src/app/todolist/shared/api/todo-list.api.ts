import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../environments/environment';
import { TodoList } from '../models/todo-list';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoListApi {

  private ressourceUrl: string = `${environment.apiUrl}/todo_lists`;

  constructor(private http: HttpClient) { }

  getAll(): Observable<TodoList[]> {
    return this.http.get(this.ressourceUrl)
      .pipe(
        map(results => results["hydra:member"])
      );
  }
}
