import { Task } from './task';

export interface TodoList {
  '@id'?: string;

  id: number;
  title: string;
  tasks: Task[];
}
