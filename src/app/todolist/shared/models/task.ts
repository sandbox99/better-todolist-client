export interface Task {
  '@id'?: string;

  id?: number;
  name?: string;
  done?: boolean;
  todolist?: string;
}
