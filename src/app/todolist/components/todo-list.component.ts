import { Task } from '../shared/models/task';
import { TodoList } from '../shared/models/todo-list';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-todo-list',
  template: `
    <h1>{{ todolist.title }}</h1>
    <div>
      <input [formControl]="newTaskName" type="text" /> <button (click)="addTask()">+</button>
    </div>
    <app-task *ngFor="let t of todolist.tasks" [task]="t" (onCheckTask)="checkTask($event)"></app-task>
  `,
  styles: [
  ]
})
export class TodoListComponent implements OnInit {

  @Input() todolist: TodoList;
  @Output() onAddTask = new EventEmitter<{task: Task, todolist: TodoList}>();
  @Output() onCheckTask = new EventEmitter<{task: Task, todolist: TodoList}>();

  newTaskName = this.fb.control('', [Validators.required, Validators.nullValidator]);

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  addTask(): void {
    if (this.newTaskName.valid) {
      let task: Task = {
        name: this.newTaskName.value
      };
      this.onAddTask.emit({
        task: task,
        todolist: this.todolist
      });

      this.newTaskName.reset();
    }
  }

  checkTask(task: Task): void {
    this.onCheckTask.emit({
      task: task,
      todolist: this.todolist
    });
  }
}
