import { FormBuilder, Validators } from '@angular/forms';
import { Task } from './../shared/models/task';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-task',
  template: `
    <div>
      {{ task.name }} <input [formControl]="name" type="checkbox" (change)="check()"/>
    </div>
  `,
  styles: [
  ]
})
export class TaskComponent implements OnInit {

  @Input() task: Task;
  @Output() onCheckTask = new EventEmitter<Task>();

  name = this.fb.control(this.task?.done, [Validators.required]);

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  check(): void {
    this.task.done = this.name.value;
    this.onCheckTask.emit(this.task);
  }
}
