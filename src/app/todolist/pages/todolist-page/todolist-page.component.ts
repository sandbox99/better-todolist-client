import { TodoListService } from './../../shared/services/todo-list.service';
import { Task } from './../../shared/models/task';
import { TodoList } from './../../shared/models/todo-list';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todolist-page',
  templateUrl: './todolist-page.component.html',
  styleUrls: ['./todolist-page.component.scss']
})
export class TodolistPageComponent implements OnInit {

  todolists: TodoList[];

  constructor(
    private route: ActivatedRoute,
    private todolistService: TodoListService
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe((data: { todolists: TodoList[] }) => this.todolists = data.todolists);
  }

  addTask(event: {task: Task, todolist: TodoList}): void {
    const { task, todolist } = event;

    this.todolistService.addTask(task, todolist)
      .subscribe((createdTask: Task) => {
        todolist.tasks.push(createdTask);
      });
  }

  checkTask(event: {task: Task, todolist: TodoList}): void {
    const { task, todolist } = event;

    this.todolistService.checkTask(task, todolist)
      .subscribe((updatedTask: Task) => {
        console.log("Task updated");
      });
  }
}
